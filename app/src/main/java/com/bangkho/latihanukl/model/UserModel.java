package com.bangkho.latihanukl.model;

/**
 * Created by BangKho on 5/5/2018.
 */

public class UserModel {
    int id;
    String username, password, nama, branch;

    public UserModel(int id, String nama, String username, String password, String branch) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.branch = branch;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getNama() {
        return nama;
    }

    public String getBranch() {
        return branch;
    }
}
