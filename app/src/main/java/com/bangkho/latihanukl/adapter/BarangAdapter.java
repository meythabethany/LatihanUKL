package com.bangkho.latihanukl.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bangkho.latihanukl.R;
import com.bangkho.latihanukl.model.BarangModel;

import java.util.List;

/**
 * Created by BangKho on 5/6/2018.
 */

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {
    String kategori, type;
    private List<BarangModel> barangList;

    public BarangAdapter(List<BarangModel> barangList) {
        this.barangList = barangList;
    }

    @Override
    public BarangAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.barang_list_ui, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BarangAdapter.ViewHolder holder, int position) {
        BarangModel barang = barangList.get(position);
        holder.namaTitle.setText(barang.getNama());
        if (barang.getIdCategory().equals("A")) {
            kategori = "Elektronik";
        } else {
            kategori = "Non-Elektronik";
        }
        switch (barang.getIdType()) {
            case 1:
                type = "Laptop";
                break;
            case 2:
                type = "Desktop";
                break;
            case 3:
                type = "LCD";
                break;
            case 4:
                type = "Kamera";
                break;
            case 5:
                type = "Elektronik Lainya";
                break;
            case 6:
                type = "Meja";
                break;
            case 7:
                type = "Lemari";
                break;
            case 8:
                type = "Sofa";
                break;
            case 9:
                type = "Kursi";
                break;
            case 10:
                type = "Non-Elektronik Lainnya";
                break;
        }
        holder.kategoriText.setText(kategori);
        holder.tanggalText.setText(barang.getTgl());
        holder.typeText.setText(type);
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView namaTitle, kategoriText, tanggalText, typeText;

        public ViewHolder(View itemView) {
            super(itemView);
            namaTitle = itemView.findViewById(R.id.namaTitle);
            kategoriText = itemView.findViewById(R.id.kategori);
            tanggalText = itemView.findViewById(R.id.tanggal);
            typeText = itemView.findViewById(R.id.type);
        }
    }
}
