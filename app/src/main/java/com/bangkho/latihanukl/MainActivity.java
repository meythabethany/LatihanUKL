package com.bangkho.latihanukl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bangkho.latihanukl.adapter.BarangAdapter;
import com.bangkho.latihanukl.connection.VolleySingleton;
import com.bangkho.latihanukl.model.BarangModel;
import com.bangkho.latihanukl.model.UserModel;
import com.bangkho.latihanukl.session.SessionPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String url = "http://ukl.haidev.xyz/index.php/barang";
    TextView nama;
    Button logout;
    RecyclerView recyclerView;
    BarangAdapter adapter;
    private List<BarangModel> barangList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!SessionPreference.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        nama = findViewById(R.id.nama);
        recyclerView = findViewById(R.id.rvBarang);

        UserModel user = SessionPreference.getInstance(this).getUser();
        nama.setText(user.getNama());

        adapter = new BarangAdapter(barangList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        loadData();

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SessionPreference.getInstance(getApplicationContext()).logout();
            }
        });
    }

    private void loadData() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    barangList.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        BarangModel barang = new BarangModel(
                                obj.getInt("IDAsset"),
                                obj.getString("Name"),
                                obj.getString("Spec"),
                                obj.getString("Date"),
                                obj.getString("Room"),
                                obj.getString("IDCategory"),
                                obj.getInt("IDType"),
                                obj.getInt("ItemPrice"),
                                obj.getInt("Quantity"),
                                obj.getString("IDInventory")
                        );
                        barangList.add(barang);
                        Log.d("Data", barangList.toString());
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


}
