package com.bangkho.latihanukl.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bangkho.latihanukl.model.BarangModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BangKho on 5/5/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DBInventory";

    private static final String TABLE_BARANG = "barang";

    private static final String KEY_IDASSET = "IDAsset";
    private static final String KEY_NAMA = "Name";
    private static final String KEY_SPEC = "Spec";
    private static final String KEY_RUANG = "Room";
    private static final String KEY_IDCATEGORY = "IDCategory";
    private static final String KEY_IDTYPE = "IDType";
    private static final String KEY_TANGGAL = "Date";
    private static final String KEY_HARGA = "ItemPrice";
    private static final String KEY_SATUAN = "Quantity";
    private static final String KEY_NOINVENTARIS = "IDInventory";

    private static final String CREATE_TABLE_BARANG = "CREATE TABLE" + TABLE_BARANG + " (" +
            KEY_IDASSET + " INTEGER PRIMARY KEY," + KEY_NAMA + " CHAR(100)," +
            KEY_SPEC + " TEXT," + KEY_TANGGAL + " DATE," + KEY_RUANG + " CHAR(100)," +
            KEY_IDCATEGORY + " CHAR(1)," + KEY_IDTYPE + " INTEGER," + KEY_HARGA + " INTEGER," +
            KEY_SATUAN + " INTEGER, " + KEY_NOINVENTARIS + " CHAR(100))";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_BARANG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BARANG);
    }

    public void insertBarang(BarangModel barang) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IDASSET, barang.getId());
        values.put(KEY_NAMA, barang.getNama());
        values.put(KEY_SPEC, barang.getSpek());
        values.put(KEY_TANGGAL, barang.getTgl());
        values.put(KEY_RUANG, barang.getRuang());
        values.put(KEY_IDCATEGORY, barang.getIdCategory());
        values.put(KEY_IDTYPE, barang.getIdType());
        values.put(KEY_HARGA, barang.getHarga());
        values.put(KEY_SATUAN, barang.getSatuan());
        values.put(KEY_NOINVENTARIS, barang.getIdInventaris());

        db.insert(TABLE_BARANG, null, values);
    }

    public List<BarangModel> getBarang() {
        List<BarangModel> barangList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_BARANG;

        Log.e("Query :", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {

                BarangModel barang = new BarangModel(
                        c.getInt(c.getColumnIndex(KEY_IDASSET)),
                        c.getString(c.getColumnIndex(KEY_NAMA)),
                        c.getString(c.getColumnIndex(KEY_SPEC)),
                        c.getString(c.getColumnIndex(KEY_TANGGAL)),
                        c.getString(c.getColumnIndex(KEY_RUANG)),
                        c.getString(c.getColumnIndex(KEY_IDCATEGORY)),
                        c.getInt(c.getColumnIndex(KEY_IDTYPE)),
                        c.getInt(c.getColumnIndex(KEY_HARGA)),
                        c.getInt(c.getColumnIndex(KEY_SATUAN)),
                        c.getString(c.getColumnIndex(KEY_NOINVENTARIS))
                );
                barangList.add(barang);
            } while (c.moveToNext());
        }
        return barangList;
    }

    public int updateToDo(BarangModel barang) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAMA, barang.getNama());
        values.put(KEY_SPEC, barang.getSpek());
        values.put(KEY_TANGGAL, barang.getTgl());
        values.put(KEY_RUANG, barang.getRuang());
        values.put(KEY_IDCATEGORY, barang.getIdCategory());
        values.put(KEY_IDTYPE, barang.getIdType());
        values.put(KEY_HARGA, barang.getHarga());
        values.put(KEY_SATUAN, barang.getSatuan());
        values.put(KEY_NOINVENTARIS, barang.getIdInventaris());

        // updating row
        return db.update(TABLE_BARANG, values, KEY_IDASSET + " = ?",
                new String[]{String.valueOf(barang.getId())});
    }

    public void deleteBarang(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BARANG, KEY_IDASSET + " = ?",
                new String[]{String.valueOf(id)});
    }
}
