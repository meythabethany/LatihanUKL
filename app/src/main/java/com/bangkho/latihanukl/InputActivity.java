package com.bangkho.latihanukl;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bangkho.latihanukl.connection.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InputActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private final String url_cat = "http://ukl.haidev.xyz/index.php/kategori";
    private final String url_type = "http://ukl.haidev.xyz/index.php/jenis";
    Spinner spinner1, spinner2;
    ArrayList<String> category = new ArrayList<String>();
    ArrayList<String> elektronik = new ArrayList<String>();
    ArrayList<String> nonElektronik = new ArrayList<String>();
    ArrayAdapter<String> dataAdapter2;
    ArrayAdapter<String> dataAdapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        spinner1.setOnItemSelectedListener(this);

        dataAdapter1 = new ArrayAdapter<String>(InputActivity.this, android.R.layout.simple_spinner_dropdown_item, category);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        dataFill();

        spinner1.setAdapter(dataAdapter1);
    }

    private void dataFill() {
        category.add("Pilih Kategori");
        category.add("Elektronik");
        category.add("Non-Elektronik");

        elektronik.add("Pilih Barang Elektronik");
        elektronik.add("Laptop");
        elektronik.add("Desktop");
        elektronik.add("LCD");
        elektronik.add("Kamera");
        elektronik.add("Elektronik Lainnya");

        nonElektronik.add("Pilih Barang Non-Elektronik");
        nonElektronik.add("Meja");
        nonElektronik.add("Lemari");
        nonElektronik.add("Sofa");
        nonElektronik.add("Kursi");
        nonElektronik.add("Non-Elektronik Lainnya");

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String selectedItem = parent.getItemAtPosition(position).toString();
        switch (selectedItem) {
            case "Elektronik":
                setadapter(0);
                break;
            case "Non-Elektronik":
                setadapter(1);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private void setadapter(int a) {
        switch (a) {
            case 0:
                dataAdapter2 = new ArrayAdapter<String>(InputActivity.this, android.R.layout.simple_spinner_dropdown_item, elektronik);
                break;
            case 1:
                dataAdapter2 = new ArrayAdapter<String>(InputActivity.this, android.R.layout.simple_spinner_dropdown_item, nonElektronik);
                break;
        }
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter2);
    }

    private void loadCategory() {
        category.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_cat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    category.add("Pilih Kategori");
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        category.add(obj.getString("Category"));
                    }
                    Log.d("Data", category.toString());
                    spinner1.setAdapter(dataAdapter1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void loadType() {
        elektronik.clear();
        nonElektronik.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_type, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    elektronik.add("Pilih Elektronik");
                    nonElektronik.add("Pilih Non-Elektronik");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        if (obj.getString("IDCategory").equals("A")) {
                            elektronik.add(obj.getString("Type"));
                        } else if (obj.getString("IDCategory").equals("B")) {
                            nonElektronik.add(obj.getString("Type"));
                        }
                    }
                    Log.d("Data", elektronik.toString());
                    Log.d("Data", nonElektronik.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

}
